require 'rubygems'
require 'sinatra'
require 'sinatra/reloader' if development?
require 'logger'
require 'unicorn'
require 'slim'

class MainController < Sinatra::Base
  # views/index.haml
  get '/' do
    slim :index
  end
end
